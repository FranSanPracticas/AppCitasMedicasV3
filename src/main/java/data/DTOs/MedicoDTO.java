package data.DTOs;

import data.DTOs.Child.PacienteChildDTO;

import java.util.LinkedList;
import java.util.List;

public class MedicoDTO {

    private int id;
    private String nombre;
    private String apellidos;
    private String usuario;
    private String clave;

    private String numColegiado;

    private List<CitaDTO> citas;
    private List<PacienteChildDTO> pacientes;



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }





    public String getNumColegiado() {
        return numColegiado;
    }
    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }



    public List<CitaDTO> getCitas() {
        return new LinkedList<CitaDTO>(citas);
    }
    public void setCitas(List<CitaDTO> citas) {
        this.citas = citas;
    }




    public List<PacienteChildDTO> getPacientes() {
        return new LinkedList<PacienteChildDTO>(pacientes);
    }
    public void setPaciente(List<PacienteChildDTO> pacientes) {
        this.pacientes = pacientes;
    }



}