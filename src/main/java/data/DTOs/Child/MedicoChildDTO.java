package data.DTOs.Child;

import data.DTOs.UsuarioDTO;

public class MedicoChildDTO extends UsuarioDTO {

    private String numColegiado;

    public String getNumColegiado() {
        return numColegiado;
    }
    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }

}
