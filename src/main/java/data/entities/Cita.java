package data.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="CITA")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
//@JsonIdentityReference(alwaysAsId=true)
public class Cita {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="id")
    private int id;
    @Column(name="fechaHora")
    private Date fechaHora;
    @Column(name="motivoCita")
    private String motivoCita;


    @ManyToOne
    @JoinColumn(name="paciente")
    private Paciente paciente;




    @ManyToOne
    @JoinColumn(name="medico")
    private Medico medico;

    //@Column(name="diagnostico")
    /*
    @OneToOne
    @JoinTable(name="cita_diagnostico",
    joinColumns = {@JoinColumn(name="cita_id", referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name="diagnostico_id", referencedColumnName = "id")})
     */

    @OneToOne
    @JoinColumn(name="diagnostico_id")
    private Diagnostico diagnostico;


    public Cita(){
    }

    public Cita(Date fechaHora, String motivoCita, Paciente paciente, Medico medico, Diagnostico diagnostico) {
        super();
        this.fechaHora = fechaHora;
        this.motivoCita = motivoCita;
        this.paciente = paciente;
        this.medico = medico;
        this.diagnostico = diagnostico;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getMotivoCita() {
        return motivoCita;
    }

    public void setMotivoCita(String motivoCita) {
        this.motivoCita = motivoCita;
    }


    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }





    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }


    public Diagnostico getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }


}
