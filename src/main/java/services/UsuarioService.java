package services;

import data.DTOs.UsuarioDTO;
import data.entities.Usuario;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.UsuarioRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService implements IUsuarioService {

    @Autowired
    UsuarioRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private UsuarioDTO mapToDTO(Usuario obj){
        UsuarioDTO dto = mapper.map(obj, UsuarioDTO.class);
        return dto;
    }

    private Usuario mapToEntity(UsuarioDTO dto){
        Usuario obj = mapper.map(dto, Usuario.class);
        return obj;
    }

    @Override
    public List<UsuarioDTO> getAll() {
        Iterable<Usuario> lista = repo.findAll();
        List<UsuarioDTO> listaDTO = new LinkedList<UsuarioDTO>();
        for (Usuario usr: lista) {
            listaDTO.add(mapToDTO(usr));
        }
        return listaDTO;
    }

    @Override
    public UsuarioDTO getById(int id) {
        if(!repo.existsById(id)){
            System.out.println("El usuario con id " + id + " no existe.");
            return null;
        } else {
            Optional<Usuario> opt = repo.findById(id);
            return mapToDTO(opt.get());
        }
    }

    @Override
    public UsuarioDTO add(UsuarioDTO obj) {
        repo.save(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) {
        if(!repo.existsById(id)){
            System.out.println("El usuario con id " + id + " no existe.");
        } else {
            repo.deleteById(id);
        }
    }

    @Override
    public void update(UsuarioDTO obj) {
        //TODO: Implementar
    }
}

