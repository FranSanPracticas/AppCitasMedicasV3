package services;

import data.DTOs.MedicoDTO;

import java.util.List;

public interface IMedicoService {

    public List<MedicoDTO> getAll();
    public MedicoDTO getById(int id);
    public MedicoDTO add(MedicoDTO obj);
    public void delete(int id);
    public void update(MedicoDTO obj);
}