package services;

import data.DTOs.DiagnosticoDTO;

import java.util.List;

public interface IDiagnosticoService {

    public List<DiagnosticoDTO> getAll();
    public DiagnosticoDTO getById(int id);
    public DiagnosticoDTO add(DiagnosticoDTO obj);
    public void delete(int id);
    public void update(DiagnosticoDTO obj);
}
