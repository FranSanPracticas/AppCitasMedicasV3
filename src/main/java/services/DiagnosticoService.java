package services;

import data.DTOs.DiagnosticoDTO;
import data.entities.Diagnostico;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.DiagnosticoRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class DiagnosticoService implements IDiagnosticoService {

    @Autowired
    DiagnosticoRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private DiagnosticoDTO mapToDTO(Diagnostico obj){
        DiagnosticoDTO dto = mapper.map(obj, DiagnosticoDTO.class);
        return dto;
    }

    private Diagnostico mapToEntity(DiagnosticoDTO dto){
        Diagnostico obj = mapper.map(dto, Diagnostico.class);
        return obj;
    }

    @Override
    public List<DiagnosticoDTO> getAll() {
        Iterable<Diagnostico> lista = repo.findAll();
        List<DiagnosticoDTO> listaDTO = new LinkedList<DiagnosticoDTO>();
        for (Diagnostico diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public DiagnosticoDTO getById(int id) {
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
            return null;
        } else {
            Optional<Diagnostico> opt = repo.findById(id);
            return mapToDTO(opt.get());
        }
    }

    @Override
    public DiagnosticoDTO add(DiagnosticoDTO obj) {
        repo.save(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) {
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
        } else {
            repo.deleteById(id);
        }
    }

    @Override
    public void update(DiagnosticoDTO obj) {
        //TODO: Implementar
    }
}