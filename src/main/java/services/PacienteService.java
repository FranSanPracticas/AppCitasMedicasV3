package services;

import data.DTOs.Child.MedicoChildDTO;
import data.DTOs.Child.PacienteChildDTO;
import data.DTOs.CitaDTO;
import data.DTOs.MedicoDTO;
import data.DTOs.PacienteDTO;
import data.entities.Cita;
import data.entities.Medico;
import data.entities.Paciente;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.PacienteRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class PacienteService implements IPacienteService {


    @Autowired
    PacienteRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private PacienteDTO mapToDTO(Paciente obj){
        //PacienteDTO dto = mapper.map(obj, PacienteDTO.class);
        PacienteDTO dto = new PacienteDTO();
        dto.setId(obj.getId());
        dto.setNSS(obj.getNSS());
        dto.setNumTarjeta(obj.getNumTarjeta());
        dto.setDireccion(obj.getDireccion());
        dto.setTelefono(obj.getTelefono());
        dto.setNombre(obj.getNombre());
        dto.setApellidos(obj.getApellidos());
        dto.setUsuario(obj.getUsuario());
        dto.setClave(obj.getClave());

        List<CitaDTO> listaDTO = new LinkedList<CitaDTO>();
        List<Cita> lista = obj.getCitas();

        for (Cita entity : lista ) {
            listaDTO.add(mapper.map(entity, CitaDTO.class));
        }
        dto.setCitas(listaDTO);

        List<MedicoChildDTO> listaDTOMedicos = new LinkedList<MedicoChildDTO>();
        List<Medico> listaMedicos = obj.getMedicos();

        for (Medico entity : listaMedicos ) {
            listaDTOMedicos.add(mapper.map(entity, MedicoChildDTO.class));
        }
        dto.setMedicos(listaDTOMedicos);

        return dto;
    }

    private Paciente mapToEntity(PacienteDTO dto){
        //Paciente obj = mapper.map(dto, Paciente.class);
        Paciente obj = new Paciente();
        obj.setId(dto.getId());
        obj.setNSS(dto.getNSS());
        obj.setNumTarjeta(dto.getNumTarjeta());
        obj.setDireccion(dto.getDireccion());
        obj.setTelefono(dto.getTelefono());
        obj.setNombre(dto.getNombre());
        obj.setApellidos(dto.getApellidos());
        obj.setUsuario(dto.getUsuario());
        obj.setClave(dto.getClave());

        List<CitaDTO> listaDTO = dto.getCitas();
        List<Cita> lista = new LinkedList<Cita>();

        for (CitaDTO citaDTO : listaDTO) {
            lista.add(mapper.map(citaDTO, Cita.class));
        }
        obj.setCitas(lista);

        List<MedicoChildDTO> listaDTOMedicos = dto.getMedicos();
        List<Medico> listaMedicos = new LinkedList<Medico>();

        for (MedicoChildDTO medicoDTO : listaDTOMedicos) {
            listaMedicos.add(mapper.map(medicoDTO, Medico.class));
        }
        obj.setMedicos(listaMedicos);

        return obj;
    }


    @Override
    public List<PacienteDTO> getAll() {
        Iterable<Paciente> lista = repo.findAll();
        List<PacienteDTO> listaDTO = new LinkedList<PacienteDTO>();
        for (Paciente pac: lista) {
            listaDTO.add(mapToDTO(pac));
        }
        return listaDTO;
    }

    @Override
    public PacienteDTO getById(int id){
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
            return null;
        } else {
            Optional<Paciente> opt = repo.findById(id);
            return mapToDTO(opt.get());
        }
    }

    @Override
    public PacienteDTO add(PacienteDTO obj) {
        repo.save(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id){
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
        } else {
            repo.deleteById(id);
        }
    }

    @Override
    public void update(PacienteDTO obj) {
        //TODO: Implementar
    }
}

