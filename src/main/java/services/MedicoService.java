package services;

import data.DTOs.Child.PacienteChildDTO;
import data.DTOs.CitaDTO;
import data.DTOs.MedicoDTO;
import data.DTOs.PacienteDTO;
import data.entities.Cita;
import data.entities.Medico;
import data.entities.Paciente;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.MedicoRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicoService implements IMedicoService {

    @Autowired
    MedicoRepository repo;

    private final ModelMapper mapper = new ModelMapper();

    private MedicoDTO mapToDTO(Medico obj){
        //MedicoDTO dto = mapper.map(obj, MedicoDTO.class);
        MedicoDTO dto = new MedicoDTO();
        dto.setId(obj.getId());
        dto.setNumColegiado(obj.getNumColegiado());
        dto.setNombre(obj.getNombre());
        dto.setApellidos(obj.getApellidos());
        dto.setUsuario(obj.getUsuario());
        dto.setClave(obj.getClave());

        List<CitaDTO> listaDTO = new LinkedList<CitaDTO>();
        List<Cita> lista = obj.getCitas();

        for (Cita entity : lista ) {
            listaDTO.add(mapper.map(entity, CitaDTO.class));
        }
        dto.setCitas(listaDTO);

        List<PacienteChildDTO> listaDTOPacientes = new LinkedList<PacienteChildDTO>();
        List<Paciente> listaPacientes = obj.getPacientes();

        for (Paciente entity : listaPacientes ) {
            listaDTOPacientes.add(mapper.map(entity, PacienteChildDTO.class));
        }
        dto.setPaciente(listaDTOPacientes);


        return dto;
    }

    private Medico mapToEntity(MedicoDTO dto){
        //Medico obj = mapper.map(dto, Medico.class);
        Medico obj = new Medico();
        obj.setId(dto.getId());
        obj.setNumColegiado(dto.getNumColegiado());
        obj.setNombre(dto.getNombre());
        obj.setApellidos(dto.getApellidos());
        obj.setUsuario(dto.getUsuario());
        obj.setClave(dto.getClave());

        List<CitaDTO> listaDTO = dto.getCitas();
        List<Cita> lista = new LinkedList<Cita>();

        for (CitaDTO citaDTO : listaDTO) {
            lista.add(mapper.map(citaDTO, Cita.class));
        }
        obj.setCitas(lista);

        List<PacienteChildDTO> listaDTOPacientes = dto.getPacientes();
        List<Paciente> listaPacientes = new LinkedList<Paciente>();

        for (PacienteChildDTO pacienteDTO : listaDTOPacientes) {
            listaPacientes.add(mapper.map(pacienteDTO, Paciente.class));
        }
        obj.setPaciente(listaPacientes);

        return obj;
    }


    @Override
    public List<MedicoDTO> getAll() {
        Iterable<Medico> lista = repo.findAll();
        List<MedicoDTO> listaDTO = new LinkedList<MedicoDTO>();
        for (Medico diag: lista) {
            listaDTO.add(mapToDTO(diag));
        }
        return listaDTO;
    }

    @Override
    public MedicoDTO getById(int id){
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
            return null;
        } else {
            Optional<Medico> opt = repo.findById(id);
            return mapToDTO(opt.get());
        }
    }

    @Override
    public MedicoDTO add(MedicoDTO obj) {
        repo.save(mapToEntity(obj));
        return obj;
    }

    @Override
    public void delete(int id) {
        if(!repo.existsById(id)){
            System.out.println("El paciente con id " + id + " no existe.");
        } else {
            repo.deleteById(id);
        }
    }

    @Override
    public void update(MedicoDTO obj) {
        //TODO: Implementar
    }
}
