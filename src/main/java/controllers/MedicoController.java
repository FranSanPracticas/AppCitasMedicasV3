package controllers;

import data.DTOs.MedicoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.MedicoService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class MedicoController {


    @Autowired
    MedicoService service;

    @GetMapping("/medicos")
    public List<MedicoDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/medicos/{id}")
    public MedicoDTO getById(@PathVariable int id){
        return service.getById(id);
    }

    @PostMapping("/medicos/add")
    public MedicoDTO add(@RequestBody MedicoDTO medicoDTO){
        return service.add(medicoDTO);
    }

    @DeleteMapping("/medicos/delete/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }

    @PutMapping("/medicos/update")
    public void update(@RequestBody MedicoDTO medicoDTO){
        service.update(medicoDTO);
    }
}

