package controllers;

import data.DTOs.CitaDTO;
import data.DTOs.Mensaje;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import services.CitaService;


import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class CitaController {

    @Autowired
    CitaService service;

    @GetMapping("/citas")
    public ResponseEntity<List<CitaDTO>> getAll(){
        List<CitaDTO> lista = service.getAll();
        return new ResponseEntity(lista, HttpStatus.OK);
    }

    @GetMapping("/citas/{id}")
    public ResponseEntity<CitaDTO> getById(@PathVariable int id){
        if(!service.existsById(id)){
            return new ResponseEntity(new Mensaje("La cita con id " + id + " no existe."), HttpStatus.NOT_FOUND);
        }
        CitaDTO citaDTO = service.getById(id);
        return new ResponseEntity(citaDTO, HttpStatus.OK);
    }

    @PostMapping("/citas/add")
    public ResponseEntity<?> add(@RequestBody CitaDTO citaDTO){
        service.add(citaDTO);
        return new ResponseEntity(new Mensaje("Cita creada"), HttpStatus.OK);
    }

    @DeleteMapping("/citas/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable int id){
        if(!service.existsById(id)){
            return new ResponseEntity(new Mensaje("La cita con id " + id + " no existe."), HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity(new Mensaje("La cita ha sido eliminada"), HttpStatus.OK);
    }

    @PutMapping("/citas/update")
    public ResponseEntity<?> update(@RequestBody CitaDTO citaDTO){
        if(!service.existsById(citaDTO.getId())){
            return new ResponseEntity(new Mensaje("La cita con id " + citaDTO.getId() + " no existe."), HttpStatus.NOT_FOUND);
        }
        service.update(citaDTO);
        return new ResponseEntity(new Mensaje("La cita ha sido actualizada"), HttpStatus.OK);
    }
}