package controllers;

import data.DTOs.DiagnosticoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import services.DiagnosticoService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class DiagnosticoController {


    @Autowired
    DiagnosticoService service;

    @GetMapping("/diagnosticos")
    public List<DiagnosticoDTO> getAll(){
        return service.getAll();
    }

    @GetMapping("/diagnosticos/{id}")
    public DiagnosticoDTO getById(@PathVariable int id){
        return service.getById(id);
    }

    @PostMapping("/diagnosticos/add")
    public DiagnosticoDTO add(@RequestBody DiagnosticoDTO diagnosticoDTO){
        return service.add(diagnosticoDTO);
    }

    @DeleteMapping("/diagnosticos/delete/{id}")
    public void delete(@PathVariable int id){
        service.delete(id);
    }

    @PutMapping("/diagnosticos/update")
    public void update(@RequestBody DiagnosticoDTO diagnosticoDTO){
        service.update(diagnosticoDTO);
    }
}

