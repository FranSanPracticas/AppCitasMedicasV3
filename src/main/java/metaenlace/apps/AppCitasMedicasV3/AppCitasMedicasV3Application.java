package metaenlace.apps.AppCitasMedicasV3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//error entityManagerFactory

//Hecho
//@EnableJpaRepositories(basePackages = "metaenlace.apps.AppCitasMedicasV2.repositories")
//@ComponentScan("metaenlace.apps.AppCitasMedicasV2.controllers")
//@ComponentScan("metaenlace.apps.AppCitasMedicasV2.services")

//Probado
//@SpringBootApplication(exclude = {ShiroAnnotationProcessorAutoConfiguration.class, ShiroAutoConfiguration.class, ShiroBeanAutoConfiguration.class}) Para anidar

//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
//@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class})

//@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class})
//@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class})



@SpringBootApplication
@Configuration
@EnableConfigurationProperties
@EnableJpaRepositories(basePackages = "repositories")
@EntityScan("data.entities")
@ComponentScan("controllers")
@ComponentScan("services")
public class AppCitasMedicasV3Application {

	public static void main(String[] args) {
		SpringApplication.run(AppCitasMedicasV3Application.class, args);
	}

}
